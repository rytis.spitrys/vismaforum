// all DOM strings
const DOMStrings = {
    customNav: 'custom-nav',
    mainNav: 'main-nav',
    logDiv: 'log',
    hamEle: 'hamNav',
};

const element = document.getElementById(DOMStrings.customNav);
const navEle = document.getElementById(DOMStrings.mainNav);
const regEle = document.getElementById(DOMStrings.logDiv);
const hamburger = document.getElementById(DOMStrings.hamEle);

window.onload = function () { 
    element.style.display = 'none';
    navEle.style.display = 'none';
    regEle.style.display = 'none';
 };

 hamburger.addEventListener("click", function() {
    element.classList.toggle("change");
    if (element.style.display === "none") {
        element.style.display = "flex";
        navEle.style.display = "flex";
        regEle.style.display = "flex";
    } else {
        element.style.display = "none";
        navEle.style.display = "none";
        regEle.style.display = "none";
    }
 });

 



